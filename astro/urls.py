from django.conf.urls import patterns, include, url

from django.contrib import admin
from parse import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'astro.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', views.index, name='index'),
    url(r'id/(?P<stuff_id>\d+)/$', views.stuff, name='stuff'),
    url(r'^admin/', include(admin.site.urls)),
)
