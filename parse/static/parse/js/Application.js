function getURLParameter(name, address) {
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(address)||[,""])[1].replace(/\+/g, '%20')) || null
}


$(document).ready(function() {
	$('#myGallery').galleryView({
		panel_scale : "fit"
	});

	$("#doSearch").click(function() {
		txt = $("#searchText").val();
		var reg = /^\d+$/;
		if (reg.test(txt)) {
			/*
			 * If i have id - just go to parser
			 */
			address = "/id/" + txt;
			window.location = address;
			return false;
		} else {
			/*
			 * if url - get id from url and go to parser
			 */
			if (txt.indexOf("classified_id")) {
				stuff_id = getURLParameter("classified_id", txt);
				if (reg.test(stuff_id)) {
					window.location = "/id/" + stuff_id;
					return false;
				};
			};
		};
		return false;
	});
	//console.log(document.documentElement);
	document.documentElement.innerHTML.replace(/(<br>*)/g, "");

	$(document).find("br").each(function() {
		if (this.previousSibling && this.previousSibling.nodeName.toUpperCase() == 'BR') {
			$(this).remove();
		}
	});
	$("td").each(function(a, b) {
		if ( typeof (b.innerText) != 'undefined') {
			/*
			 * WebKit
			 */			
			if (b.textContent.indexOf("S  H") == 0) {
				b.textContent = "S & H:";
			};
			if (b.textContent === "") {
				b.remove();
			};
		} else {
			/*
			 * FireFox
			 */			
			if (b.textContent.indexOf("S  H") == 0) {
				b.textContent = "S & H:";
			};
			if (b.textContent === "") {
				b.remove();
			};
		}

	});
});
