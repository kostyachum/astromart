from django.shortcuts import render
from HTMLParser import HTMLParser
import urllib2

DUMMY = "http://www.astromart.com/classifieds/printerdetails.asp?classified_id="
DUMMY_SOLD = "http://www.astromart.com/classifieds/details.asp?classified_id="
parsed_data = {}

# Create your views here.
def index(request):
    return render(request, 'parse/index.html')
 
def stuff(request, stuff_id):
    context = {"stuff_id":stuff_id}
    address = ("%s%s") % (DUMMY, stuff_id)
    address_sold = ("%s%s") % (DUMMY_SOLD, stuff_id)
    website = urllib2.urlopen(address)
    website_html = website.read()
    
    website_sold = urllib2.urlopen(address_sold)
    website_html_sold = website_sold.read()
    try:
        website = urllib2.urlopen(address)
        website_sold = urllib2.urlopen(address_sold)
    except urllib2.HTTPError, e:
        print "Cannot retrieve URL: HTTP Error Code", e.code
    except urllib2.URLError, e:
        print "Cannot retrieve URL: " + e.reason[1]
        
    parser = MyHTMLParser()
    parser.feed(website_html)
    
    if "The classified you requested could not be found." in website_html:
        context['not_found'] = True
    parser_sold = MyHTMLSoldParser()
    parser_sold.feed(website_html_sold)
    
    context['price'] = parser.res['price']
    context['seller'] = parser.res['seller']
    context['location'] = parser.res['location']
    context['sh'] = parser.res['sh']
    context['title'] = parser.res['title']
    context['desc'] = parser.res['desc']
    context['images'] = parser.images
    context['details'] = parser.res['details']
    context['all'] = parser.res
    context['sold'] = parser_sold.res['title']
    return render(request, 'parse/index.html', context)


class MyHTMLParser(HTMLParser): 
    def __init__(self):
        HTMLParser.__init__(self)
        self.line = 0
        self.img = 0
        self.label = 0
        self.value = 0
        self.ul = 0
        self.title = 0
        self.desc = 0
        self.deteils = 0
        self.ignore = 0
        self.res = {}
        self.res['title'] = ""
        self.res['desc'] = ""
        self.res['seller'] = ""
        self.res['price'] = ""
        self.res['location'] = ""
        self.res['sh'] = ""
        self.res['details'] = ""
        self.images = []
        
    def handle_starttag(self, tag, attrs):
        """
        Define Value of Label
        """
        if tag == "td":
            for name, value in attrs:
                if name == "class" and value == "Value":
                    self.value = 1
                if name == "class" and value == "Label":                  
                    self.label = 1
        """
        Image list start
        """                 
        if tag == "ul":
            self.ul = 1
        
        """
        Title start
        """
        if tag == "span":
            for name, value in attrs:
                if name == "class" and value == "ClassifiedHeader":
                    self.title = 1
        """
        Tables with description and details
        """
        if tag == "table":
            for name, value in attrs:
                if name == "class" and value == "ClassifiedDescription":
                    self.desc = 1                    
                if name == "class" and value == "ClassifiedDetails":
                    self.deteils = 1
        """
        Current image
        """
        if self.ul and tag == "img":
            for img in attrs:
                self.images.append(img[1])
            self.img = 1
        
        if self.desc:
                self.res['desc'] = ("%s<%s>") % (self.res['desc'], tag)
        
        if self.deteils and tag != "a":
            if tag == "td":
                if self.value:
                    self.res['details'] = ("%s<%s class='aa'>") % (self.res['details'], tag)
                if self.label:
                    self.res['details'] = ("%s<%s class='bb'>") % (self.res['details'], tag)
            else:
                if tag == "img":
                    self.res['details'] = ("%s<img src='http://www.astromart.com/%s'>") % (self.res['details'],attrs[0][1])
                else:
                    self.res['details'] = ("%s<%s>") % (self.res['details'], tag)

    def handle_endtag(self, tag):
        if tag == 'ul':
            #image list end
            self.ul = 0
            
        if self.title:
            #title end
            self.title = 0
        
        if self.desc:
            if tag == "table":
                self.desc = 0
            self.res['desc'] = ("%s</%s>") % (self.res['desc'], tag)
        
        if self.deteils and tag != "a" and self.ignore == 0:
            if tag == "table":
                self.deteils = 0
            self.res['details'] = ("%s</%s>") % (self.res['details'], tag)
                
        if tag == "td":
            if self.value:
                self.value = 0
                self.label = 1
                self.ignore = 0
            else:
                self.value = 1
                self.label = 0

    def handle_data(self, data):
        if self.label:
            if "Location" in data:
                self.ignore = 1
            if "Payment" in data:
                self.ignore = 1

        if self.desc:
            """ Fill description"""
            self.res['desc'] = ("%s%s") % (self.res['desc'], data)
            
        if self.title:
            """ Fill Title"""
            self.res['title'] = ("%s%s") % (self.res['title'], data.strip())
                                        
        if self.deteils and self.ignore == 0:
            """ Fill deteils"""          
            self.res['details'] = ("%s%s") % (self.res['details'], data)


class MyHTMLSoldParser(HTMLParser): 
    def __init__(self):
        HTMLParser.__init__(self)    
        self.title = 0  
        self.res={}    
        self.res['title'] = ""
        print "sold parser"    
        
    def handle_starttag(self, tag, attrs):
        """
        Title start
        """
        if tag == "span":
            for name, value in attrs:
                if name == "class" and value == "ClassifiedStatusSold":
                    self.title = 1
    

    def handle_endtag(self, tag):            
        if self.title:
            #title end
            self.title = 0      

    def handle_data(self, data):
        if self.title:
            """ Fill Title"""
            self.res['title'] = ("%s%s") % (self.res['title'], data.strip())
                                        
      
            
       
           


